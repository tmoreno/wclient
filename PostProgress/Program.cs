﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PostProgress
{
    class Program
    {
        static void Main(string[] args)
        {
            int bytesRead = 0;
            int hullServerId = 1;
            string hash = "0";
            string filename = "testfile.zip";

            if (args.Length > 0)
            {
                bytesRead = Int32.Parse(args[0]);
            }

            if (args.Length > 1)
            {
                hullServerId = Int32.Parse(args[1]);
            }

            if (args.Length > 2)
            {
                hash = args[2];
            }

            if (args.Length > 3)
            {
                filename = args[3];
            }

            string json = JsonConvert.SerializeObject(new
            {
                HULL_SERVER_ID = hullServerId,
                FileName = filename,
                Date = DateTime.Now,
                Hash = hash,
                Progress = bytesRead
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59034/api/AfloatFileHullServers", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            Console.WriteLine(jsonString.Result);
        }
    }
}
