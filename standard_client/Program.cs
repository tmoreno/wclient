﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Timers;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.Security.Cryptography;
using NLog;

namespace standard_client
{


    class MainClass
    {
        //private int pct = 0;
        //private int lastPr = 0;

        //private bool done = false;

        static Logger logger = LogManager.GetCurrentClassLogger();

        private string id = "1";
        private string Filename { get; set; }
        private bool done = false;

        public string Id { get => id; set => id = value; }

        public MainClass(string filename)
        {
            this.Filename = filename;

        }

        static void Main(string[] args)
        {
            string filename = "testfile.zip";

            if(args.Length>0)
            {
                filename = args[0];
                
            }

            MainClass mainClass = new MainClass(filename);

            if (args.Length > 1)
            {
                mainClass.Id = args[1];
            }

            Console.WriteLine("Starting!");
            mainClass.Download();
        }


        private void Download()
        {
            
            try
            {
                string currDir = string.Format(@"{0}\dropbox\", Environment.CurrentDirectory);
                string url = "http://localhost:59034/Download/GetFile?filename=" + this.Filename;

                Console.WriteLine(url);

                using (WebClient wc = new WebClient())
                {
                    wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                    wc.DownloadFileCompleted += wc_DownloadFileCompleted;
                    var path = currDir + this.Filename;
                    Console.WriteLine(path);
                    wc.DownloadFileAsync(new Uri(url), path);
                }
            }
            catch(Exception ex)
            {
         
                logger.Error(ex, ex.Message);

            }
           


            while (!done)
            {
                System.Threading.Thread.Sleep(100);
            }

        }

        /// <summary>
        ///  Show the progress of the download in a progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // In case you don't have a progressBar Log the value instead 

            //Console.WriteLine(e.ProgressPercentage);

            //string pattern = @"^[1-9]";
            //Match result = Regex.Match(e.ProgressPercentage.ToString(), pattern);
            //if (result.Success)
            //{
            //    int val = Int32.Parse(result.Value);
            //    Console.WriteLine(val);
            //    if (e.ProgressPercentage > 9 && e.ProgressPercentage != val)
            //    {
            //        pct = val * 10;
            //        //Console.WriteLine(e.ProgressPercentage);
            //        //progressBar1.Value = e.ProgressPercentage;
            //        if(lastPr < e.ProgressPercentage)
            //        {
            //            lastPr = pct;
            //            Console.WriteLine(e.ProgressPercentage + "% | " + e.BytesReceived + " bytes out of " + e.TotalBytesToReceive + " bytes retrieven.");
            //        }
            //    }
            //}
            
            if(e.ProgressPercentage % 5 == 0)
            {
                Console.WriteLine(string.Format("Posting {0}", e.ProgressPercentage));
                Thread.Sleep(1000);
                PostAfloatFileHullServersRestApiTest(e.BytesReceived, "0");
            }

            //Console.WriteLine(done);

            //

            //System.Timers.Timer aTimer = new System.Timers.Timer();

            //aTimer.Elapsed += delegate (object source, ElapsedEventArgs eea)
            //{
            //    PostAfloatFileHullServersRestApiTest(e.BytesReceived);
            //};

            //aTimer.AutoReset = false;
            //aTimer.Enabled = true;
            //aTimer.Interval = 5000;



        }

        private void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {


            if (e.Cancelled)
            {
                Console.WriteLine("The download has been cancelled");
                return;
            }

            if (e.Error != null) // We have an error! Retry a few times, then abort.
            {
                Console.WriteLine("An error ocurred while trying to download file");

                return;
            }

            Console.WriteLine("File succesfully downloaded");

            Thread.Sleep(1000);

            string currDir = string.Format(@"{0}\dropbox\", Environment.CurrentDirectory);

            var path = currDir + "/" + this.Filename;

            long length = new System.IO.FileInfo(path).Length;

            using (var stream = new BufferedStream(System.IO.File.OpenRead(path), 1200000))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                string sha256str = BitConverter.ToString(checksum).Replace("-", String.Empty);

                Console.WriteLine(sha256str);

                Thread.Sleep(2000);
                PostAfloatFileHullServersRestApiTest(length, sha256str);
                
                Console.WriteLine("came back");
                this.done = true;
            }
        }


        public void PostAfloatFileHullServersRestApiTest(long bytesReceived, string sha256str)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = @"C:\Users\tmoreno\work\wclient\PostProgress\bin\Debug\PostProgress.exe";
            startInfo.Arguments = bytesReceived.ToString() + " " + this.id + " " + sha256str + " " + this.Filename;
            process.StartInfo = startInfo;
            process.Start();
        }


    }
}
