﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Timers;

namespace wclient
{
    class Program
    {
        static bool done = false;

        static void Main(string[] args)
        {
            Console.WriteLine("DS Replication Simulator");
            downloadFile2();

            while(!done)
            {
                System.Threading.Thread.Sleep(100);
            }


        }

        static private string getFilename(string hreflink)
        {
            Uri uri = new Uri(hreflink);

            string filename = System.IO.Path.GetFileName(uri.LocalPath);

            return filename;
        }


        static private void downloadFile()
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            // Change the url by the value you want (a textbox or something else)
            string url = "https://www.google.com/images/icons/ui/doodle_plus/logo.png";
            // Get filename from URL
            string filename = getFilename(url);

            using (var client = new WebClient())
            {
                client.DownloadFile(url, desktopPath + "/" + filename);
            }

            Console.WriteLine("Download ready");
        }

        static private void downloadFile2()
        {

            PostAfloatFileHullServersRestApiTest(1233);
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            // This will download a large image from the web, you can change the value
            // i.e a textbox : textBox1.Text
            string url = "http://localhost:59033/Download/GetFile?filename=LINQPad5Setup.zip";
            string filename = "LINQPad5Setup.zip";

            using (WebClient wc = new WebClient())
            {

                wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                wc.DownloadFileCompleted += wc_DownloadFileCompleted;
                wc.DownloadFileAsync(new Uri(url), desktopPath + "/" + filename);
            }

            PostAfloatFileHullServersRestApiTest(2343);
        }

        /// <summary>
        ///  Show the progress of the download in a progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

            //System.Timers.Timer aTimer = new System.Timers.Timer();

            //aTimer.Elapsed += delegate (object source, ElapsedEventArgs eea)
            //{
            //};

            //aTimer.AutoReset = false;
            //aTimer.Enabled = true;
            //aTimer.Interval = 5000;

            // In case you don't have a progressBar Log the value instead 
            Console.WriteLine(e.ProgressPercentage);
            //progressBar1.Value = e.ProgressPercentage;
            Console.WriteLine(e.ProgressPercentage + "% | " + e.BytesReceived + " bytes out of " + e.TotalBytesToReceive + " bytes retrieven.");
            Console.WriteLine("1ok");
            PostAfloatFileHullServersRestApiTest(e.BytesReceived);

        }

        static public void PostAfloatFileHullServersRestApiTest(long bytesReceived)
        {
            string json = JsonConvert.SerializeObject(new
            {
                HULL_SERVER_ID = 1,
                FileName = "LINQPad5Setup.zip",
                Date = DateTime.Now,
                Hash = "234323",
                Progress = 5444
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            var response = client.PostAsync("http://localhost:59033/api/AfloatFileHullServers", content).Result;

            var jsonString = response.Content.ReadAsStringAsync();
            Console.WriteLine("ok");
            Console.WriteLine(jsonString.Result);
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            // callActivity();
        }


        static private void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            

            if (e.Cancelled)
            {
                Console.WriteLine("The download has been cancelled");
                done = true;
                return;
            }

            if (e.Error != null) // We have an error! Retry a few times, then abort.
            {

                Console.WriteLine("An error ocurred while trying to download file");
                done = true;
                return;
            }

            

            Console.WriteLine("File succesfully downloaded");
            done = true;
        }


    }
}
